# GKObstacleGraph<GKGraphNode2D> does not handle archiving. #

## Reproducibility proof project showing a bug which is reported to Apple. ##
 
## Description ##
Creating a GKObstacleGraph<GKGraphNode2D> for my game level, with about 500 obstacles, takes ~50 seconds. This is too long. 
Since the game map layout never changes dynamically after being loaded, I decided to create the graph once, archive it to file, add the file to my bundle, and then just unarchive the graph object when I load my game level. In theory, this should be much quicker since I don't have to calculate the graph, just read it from file.

However, depending on the number of obstacles and their relative placement one of three things happens:
1. NSKeyedArchiver.archiveRootObject crashes.
2. Archive works but NSKeyedUnarchiver.unarchiveObject crashes.
3. Both archive and unarchive works, but I can not find a path around obstacles with GKObstacleGraph.findPath

### Other Information ###
I can get all of this working if I skip the (un)archive steps. (I provided screenshots where I'm skipping the archive steps.)
Also, on simulator (iPhone 7), (un)archive never crashes, but path finding always fails afterwards.