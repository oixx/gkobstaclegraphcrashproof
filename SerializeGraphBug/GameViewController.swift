//
//  GameViewController.swift
//  SerializeGraphBug
//
//  Created by David Virdefors on 23/01/17.
//  Copyright © 2017 Virdefors. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {

    private var scene: GameScene!

    private func crashTest() {
        let obstacles = scenarioOne()
//        let obstacles = scenarioTwo()
//        let obstacles = scenarioThree()

        let graph = createGraph(obstacles: obstacles)

        let start = CGPoint(x: -(scene.size.width / 2), y: -200)
        let destination = CGPoint(x: (scene.size.width / 2), y: 200)

        serialize(graph: graph)

        if let unpackedGraph = unpackFromDevice() {
            printPath(start: start, destination: destination, graph: unpackedGraph)
//            printGraph(graph: graph)
        }
    }

    // iPhone 6: Archive crash.
    // Obstacles: 336, Nodes: 1334
    private func scenarioOne()  -> [GKPolygonObstacle] {
        return createObstacles(margin: 50, rows: 14, cols: 24)
    }

    // iPhone 6: Archive works. Unarchive crash.
    // Obstacles: 322, Nodes: 1288
    private func scenarioTwo() -> [GKPolygonObstacle] {
        return createObstacles(margin: 50, rows: 14, cols: 23)
    }

    // iPhone 6: Archive works. Unarchive works. Finding a path fails.
    // Obstacles: 336, Nodes: 1334
    private func scenarioThree() -> [GKPolygonObstacle] {
        return createObstacles(margin: 15, rows: 14, cols: 24)
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let scene = GameScene(fileNamed:"GameScene") {

            let skView = self.view as! SKView
            skView.ignoresSiblingOrder = true
            skView.showsPhysics = true
            scene.scaleMode = .aspectFill
            skView.presentScene(scene)
            self.scene = scene
            crashTest()
        }
    }

    private func printGraph(graph: GKObstacleGraph<GKGraphNode2D>) {
        for parentNode in graph.nodes! as! [GKGraphNode2D] {
            let start = CGPoint(x: CGFloat(parentNode.position.x) ,y: CGFloat(parentNode.position.y))
            for connectedNode in parentNode.connectedNodes as! [GKGraphNode2D] {
                let end = CGPoint(x: CGFloat(connectedNode.position.x) ,y: CGFloat(connectedNode.position.y))
                drawLine(from: start, to: end)
            }
        }
    }

    private func createGraph(obstacles: [GKPolygonObstacle]) -> GKObstacleGraph<GKGraphNode2D> {
        let graph = GKObstacleGraph(obstacles: obstacles, bufferRadius: 5, nodeClass: GKGraphNode2D.self)
        print("nodes: \(graph.nodes?.count)")
        return graph
    }

    private func createObstacles(margin: Int, rows: Int, cols: Int) -> [GKPolygonObstacle] {
        print("obstacles: \(rows * cols)")
        let start = CGPoint(x: -(scene.size.width / 2) + 50, y: -(scene.size.height / 2))
        var obstacles = [GKPolygonObstacle]()

        for row in 1...rows {
            for col in 1...cols {
                let sprite = SKSpriteNode(color: UIColor.black, size: CGSize(width: 15, height: 15))
                sprite.position = CGPoint(x: start.x + CGFloat(margin * col) , y: start.y + CGFloat(margin * row))
                let body = SKPhysicsBody(rectangleOf: sprite.size)
                body.isDynamic = false
                sprite.physicsBody = body
                scene.addChild(sprite)
                let obstacle = SKNode.obstacles(fromNodePhysicsBodies: [sprite])[0]
                obstacles.append(obstacle)
            }
        }
        return obstacles
    }

    private func drawLine(from: CGPoint, to: CGPoint) {
        let pathToDraw:CGMutablePath = CGMutablePath()
        let myLine: SKShapeNode = SKShapeNode(path:pathToDraw)
        pathToDraw.move(to: from)
        pathToDraw.addLine(to: to)
        myLine.path = pathToDraw
        myLine.strokeColor = SKColor.blue
        myLine.zPosition = 99
        myLine.alpha = 0.2
        scene.addChild(myLine)
    }

    private func unpackFromBundle() -> GKObstacleGraph<GKGraphNode2D>? {
        if let filePath = Bundle.main.path(forResource: "obstacle", ofType: "graph") {
            let graph = NSKeyedUnarchiver.unarchiveObject(withFile: filePath) as! GKObstacleGraph<GKGraphNode2D>
            return graph
        }
        return nil
    }

    private func unpackFromDevice() -> GKObstacleGraph<GKGraphNode2D>? {
        let directories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        if let documents = directories.first {
            if let filePath = String(documents + "/obstacle.graph") {
                let graph = NSKeyedUnarchiver.unarchiveObject(withFile: filePath) as! GKObstacleGraph<GKGraphNode2D>
                return graph
            }
        }
        
        return nil
    }

    private func serialize(graph: GKObstacleGraph<GKGraphNode2D>) {
        let directories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        if let documents = directories.first {
            print(documents)
            if let filePath = String(documents + "/obstacle.graph") {
                NSKeyedArchiver.archiveRootObject(graph, toFile: filePath)
            }
        }
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .landscapeLeft
        } else {
            return .all
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    fileprivate func printPath(start: CGPoint, destination: CGPoint, graph: GKObstacleGraph<GKGraphNode2D>) {
        let startPoint = float2(Float(start.x), Float(start.y))
        let startGKNode = GKGraphNode2D(point: startPoint)
        let destinationGKNode = GKGraphNode2D(point: float2(Float(destination.x), Float(destination.y)))

        graph.connectUsingObstacles(node: startGKNode, ignoring: [])
        graph.connectUsingObstacles(node: destinationGKNode, ignoring: [])

        let pathNodes = graph.findPath(from: startGKNode, to: destinationGKNode) as! [GKGraphNode2D]

        guard pathNodes.count > 0 else {
            print("No Path")
            return
        }
        let first = pathNodes.first!
        var previousPoint: CGPoint = CGPoint(x: CGFloat(first.position.x), y: CGFloat(first.position.y))
        for node in pathNodes {
            var point : CGPoint = CGPoint()
            point.x = CGFloat(node.position.x)
            point.y = CGFloat(node.position.y)
            drawLine(from: previousPoint, to: point)
            previousPoint = point
        }
        graph.remove([startGKNode, destinationGKNode])
    }
}
